var APP_SETUP = require('./config.appSetup');
var PATH = require('./config.path');

module.exports = {
    APP_SETUP: APP_SETUP,
    PATH: PATH
};
