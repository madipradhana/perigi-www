import styles from './style.css';
import ImagePreload from 'react-imageloader';
import BackgroundImage from 'react-background-image-loader';

const ParallaxSection = ({ children, backgroundImage, initialBackgroundImage, className, anchorId }) => {
  const slideStyle = className ? `${styles.slide} ${className}` : styles.slide;

  const wrapper = backgroundImage
    ? (
      <BackgroundImage src={backgroundImage} placeholder={initialBackgroundImage || backgroundImage} className={slideStyle}>
        <div className={styles.contentWrapper}>
          {children}
        </div>
      </BackgroundImage>
    )
    : (
      <div className={slideStyle}>
        <div className={styles.contentWrapper}>
          {children}
        </div>
      </div>
    );

  return (
    <section id={anchorId}>
      {wrapper}
    </section>
  );
};

ParallaxSection.defaultProps = {
  anchorId: null,
};

ParallaxSection.propTypes = {
  children: React.PropTypes.object,
  className: React.PropTypes.string,
};


export default ParallaxSection;
