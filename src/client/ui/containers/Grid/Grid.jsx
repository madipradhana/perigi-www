import styles from './style.css';

const Grid = ({ gridItems, className }) => {
  const gridStyle = className ? `${styles.container} ${className}` : styles.container;
  const _getGridItems = (items) => {
    return (items || []).map((item, index) => {
        return (
            <li className={styles.item} key={`grid-image-${index}`}>
                {item}
            </li>
        );
    });
  };

  return (
    <div className={styles.root}>
      <ul className={gridStyle}>
        {_getGridItems(gridItems)}
      </ul>
    </div>
  );
};

Grid.propTypes = {
  gridItems: React.PropTypes.array,
};

export default Grid;
