import Header from '~/src/client/ui/sections/Header';
import styles from './style.css';
import Logo from '~/src/client/assets/images/logo.png';
import { anchorService, anchorAbout, anchorWork, anchorContact } from './anchorTypes';

const navItems = [
  {
    name: 'Home',
    path: '/'
  },
  {
    name: 'Services',
    anchor: anchorService
  },
  {
    name: 'Works',
    anchor: anchorWork
  },
  {
    name: 'About',
    anchor: anchorAbout
  },
  {
    name: 'Contact',
    anchor: anchorContact
  }
].reverse();

class ViewMain extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className={styles.root}>
        <Header className={styles.header} navigationItems={navItems} brandImage={Logo} location={this.props.location} />
        <div className={styles.content}>
          {this.props.children}
        </div>
      </div>
    );
  }
}

ViewMain.propTypes = {
  children: React.PropTypes.object.isRequired,
  location: React.PropTypes.object,
};

export default ViewMain;

