import Grid from '~/src/client/ui/containers/Grid';
import GridImageItem from '~/src/client/ui/components/GridImageItem'
import styles from './style.css';

const SectionService = ({items, isLoading = true}) => {
  const _renderPage = function(items) {
    return items.map((item, index) => {
      return (<GridImageItem key={`service-${index}`} gridImage={item.icon} className={styles.gridImageCard}>{item.title}</GridImageItem>);
    });
  };
  const _renderLoading = function() {
    return (<div>Loading...</div>);
  }

  let rendered = _renderLoading();
  if (!isLoading) {
    if (items.length > 0) {
      rendered =  <Grid gridItems={_renderPage(items)} className={styles.gridServices} />;
    } else {
      rendered =  <div>Failed to retrieve data</div>;
    }
  }

  return (
    <div className={styles.service}>
      <h1>Our Service</h1>
      {rendered}
    </div>
  );
};

export default SectionService;
