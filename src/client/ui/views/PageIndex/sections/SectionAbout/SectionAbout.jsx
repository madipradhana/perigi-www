import Logo from '~/src/client/assets/images/about-logo.png';
import styles from './style.css';

const SectionAbout = ({ contents, contacts, isLoading }) => {
  const _renderPage = function(contents) {
    return contents.map((content, index) => {
      return (<p key={`paragraph-${index}`}>{content}</p>);
    });
  };
  const _renderAsidePage = function({ info, phone, fax, email, workHours }) {
    const open = workHours.open.map((data, index) => {
      return (<p key={`open-${index}`}>{data}</p>);
    });

    return (
      <div>
        <p className={styles.info}>{info}</p>
        <h3>INFO</h3>
        <div className={styles.contactInfo}>
          {phone ? <p>{`Phone: ${phone}`}</p> : null}
          {fax ? <p>{`Fax: ${fax}`}</p> : null}
          {email ? <p>{`Email: ${email}`}</p> : null}
        </div>
        <h3>WORK HOURS</h3>
        <div className={styles.workHours}>
          <div>{open}</div>
          <div>{workHours.close.join()}: CLOSED</div>
        </div>
      </div>
    );
  };

  const _renderLoading = function() {
    return (<div>Loading...</div>);
  }

  let mainRendered = _renderLoading();
  let asideRendered = _renderLoading();
  if (!isLoading) {
    if (contents.length > 0) {
      mainRendered =  _renderPage(contents);
      asideRendered = _renderAsidePage(contacts);
    } else {
      mainRendered =  <div>Failed to retrieve data</div>;
      asideRendered = _renderAsidePage(contacts);
    }
  }

  return (
    <ul className={styles.about}>
      <li>
        <aside>
          <div className={styles.logo}><img src={Logo} /></div>
          <div className={styles.contact}>
            {asideRendered}
          </div>
        </aside>
      </li>
      <li>
        <h1>About Us</h1>
        <article>
          {mainRendered}
        </article>
      </li>
    </ul>
  );
};

export default SectionAbout;
