import Button from '~/src/client/ui/components/Button';
import styles from './style.css';

const SectionHeading = () => {
  return (
    <div className={styles.heading}>
      <div className={styles.container}>
        <h2>WE CREATE</h2><br/>
        <h1>80+ PROJECTS</h1>
        <div className={styles.buttonWrapper}>
          <Button className={styles.button}>ADD MORE PROJECT</Button>
        </div>
      </div>
    </div>
  );
};

export default SectionHeading;
