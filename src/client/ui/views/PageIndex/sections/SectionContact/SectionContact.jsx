import { GoogleMapLoader, GoogleMap, Marker } from "react-google-maps";
import Button from '~/src/client/ui/components/Button';
import styles from './style.css';

const SectionContact = (props) => {
  const myLatLng = { lat: -6.247675, lng: 106.819142 };
  const myPosition = { lat: -6.247675, lng: 106.849142 };
  const _click_from_children_of_infoBox = (e) => {
    console.log(`_click_from_children_of_infoBox!!`);
    console.log(e);
  };

  return (
    <div className={styles.contactContainer}>
      <GoogleMapLoader
        containerElement={
          <div className={styles.map} />
        }
        googleMapElement={
          <GoogleMap
            ref={(map) => console.log(map)}
            defaultZoom={14}
            maxZoom={15}
            defaultCenter={myPosition}
            draggable={false}
            onClick={props.onMapClick}
          >
            <Marker
              position={myLatLng}
              clickable={false}
              draggable={false}
            />
          </GoogleMap>
        }
      />

      <div className={styles.contact}>
        <div className={styles.contactWrapper}>
          <section>
            <div className={styles.contactForm}>
              <h1>CONTACT US</h1>
              <div className={styles.formArea}>
                <input type="text" /><div className={styles.formCaption}>Full Name</div>
              </div>
              <div className={styles.formArea}>
                <input type="text" /><div className={styles.formCaption}>Email</div>
              </div>
              <div className={styles.formArea}>
                <textarea></textarea><div className={styles.formCaption}>Message</div>
              </div>
              <div className={styles.formArea}>
                <Button className={styles.button}>
                  Send
                </Button>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
  );
};

export default SectionContact;
