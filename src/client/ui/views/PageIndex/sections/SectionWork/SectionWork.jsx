import BackgroundImage from 'react-background-image-loader';
import Lightbox from 'react-images';
import IsMobile from 'ismobilejs';
import Button from '~/src/client/ui/components/Button';
import styles from './style.css';
import Residetials1 from '~/src/client/assets/images/uploads/residentials-1.jpg';
import Residetials2 from '~/src/client/assets/images/uploads/residentials-2.jpg';
import Residetials3 from '~/src/client/assets/images/uploads/residentials-3.jpg';
import Residetials4 from '~/src/client/assets/images/uploads/residentials-4.jpg';
import Residetials5 from '~/src/client/assets/images/uploads/residentials-5.jpg';
import Residetials6 from '~/src/client/assets/images/uploads/residentials-6.jpg';
import Offices1 from '~/src/client/assets/images/uploads/offices-1.jpg';
import Offices2 from '~/src/client/assets/images/uploads/offices-2.jpg';
import Residetials1Mobile from '~/src/client/assets/images/uploads/residentials-1-mobile.jpg';
import Residetials2Mobile from '~/src/client/assets/images/uploads/residentials-2-mobile.jpg';
import Residetials3Mobile from '~/src/client/assets/images/uploads/residentials-3-mobile.jpg';
import Residetials4Mobile from '~/src/client/assets/images/uploads/residentials-4-mobile.jpg';
import Residetials5Mobile from '~/src/client/assets/images/uploads/residentials-5-mobile.jpg';
import Residetials6Mobile from '~/src/client/assets/images/uploads/residentials-6-mobile.jpg';
import Offices1Mobile from '~/src/client/assets/images/uploads/offices-1-mobile.jpg';
import Offices2Mobile from '~/src/client/assets/images/uploads/offices-2-mobile.jpg';

class SectioWork extends React.Component {
  static _renderLoading() {
    return (<div>Loading...</div>);
  }

  static _renderImages(items) {
    return items.map((item, index) => {
      return (
        <li key={`workImage-${index}`}>
          <BackgroundImage
            src={item.backgroundImage}
            placeholder={item.backgroundImage || item.backgroundImage}
            className={styles.images}
          />
        </li>
      );
    });
  }

  constructor(props) {
    super(props);

    const images = this.props.data.map((item) => {
      return (
        { src: item.backgroundImage }
      );
    });

    this.state = {
      isLoading: true,
      data: this.props.data,
      showItemDetail: null,
      showGallery: false,
      lightboxIsOpen: false,
      currentLightboxImage: 0,
      currentLightboxGalleryImagesSelected: 0,
      lightBoxGalleryImages: [
        [
          { src: !IsMobile ? Residetials1 : Residetials1Mobile },
          { src: !IsMobile ? Residetials2 : Residetials2Mobile },
          { src: !IsMobile ? Residetials3 : Residetials3Mobile },
          { src: !IsMobile ? Residetials4 : Residetials4Mobile },
          { src: !IsMobile ? Residetials5 : Residetials5Mobile },
          { src: !IsMobile ? Residetials6 : Residetials6Mobile },
        ],
        [
          { src: !IsMobile ? Offices1 : Offices1Mobile },
          { src: !IsMobile ? Offices2 : Offices2Mobile },
        ],
        [
          { src: !IsMobile ? Residetials1 : Residetials1Mobile },
          { src: !IsMobile ? Residetials2 : Residetials2Mobile },
          { src: !IsMobile ? Residetials3 : Residetials3Mobile },
          { src: !IsMobile ? Residetials4 : Residetials4Mobile },
          { src: !IsMobile ? Residetials5 : Residetials5Mobile },
          { src: !IsMobile ? Residetials6 : Residetials6Mobile },
        ],
        [
          { src: !IsMobile ? Offices1 : Offices1Mobile },
          { src: !IsMobile ? Offices2 : Offices2Mobile },
        ]
      ],
    };

    this.handleMouseOver = this.handleMouseOver.bind(this);
    this.handleMouseOutOfSection = this.handleMouseOutOfSection.bind(this);
    this.handleOpenGallery = this.handleOpenGallery.bind(this);
    this._renderData = this._renderData.bind(this);

    this.closeLightbox = this.closeLightbox.bind(this);
    this.gotoNextLightboxImage = this.gotoNextLightboxImage.bind(this);
    this.gotoPrevLightboxImage = this.gotoPrevLightboxImage.bind(this);
    this.gotoLightboxImage = this.gotoLightboxImage.bind(this);
    this.handleClickImage = this.handleClickImage.bind(this);
    this.openLightbox = this.openLightbox.bind(this);
  }

  handleMouseOutOfSection() {
    this.setState({ showItemDetail: null });
  }

  handleMouseOver(itemIndex) {
    this.setState({ showItemDetail: itemIndex });
  }

  handleOpenGallery() {
    this.setState({ showGallery: true });
  }

  openLightbox(index) {
    this.setState({
      currentLightboxGalleryImagesSelected: index,
      lightboxIsOpen: true,
    });
  }

  closeLightbox() {
    this.setState({
      currentLightboxImage: 0,
      lightboxIsOpen: false,
    });
  }

  gotoPrevLightboxImage() {
    this.setState({
      currentLightboxImage: this.state.currentLightboxImage - 1,
    });
  }

  gotoNextLightboxImage() {
    this.setState({
      currentLightboxImage: this.state.currentLightboxImage + 1,
    });
  }

  gotoLightboxImage(index) {
    this.setState({
      currentLightboxImage: index,
    });
  }

  handleClickImage() {
    if (this.state.currentLightboxImage === this.props.images.length - 1) return;

    this.gotoNext();
  }

  _renderBackgroundData(items) {
    return items.map((item, index) => {
      const addingIndex = index % 2 !== 0 ? -1 : 1;
      const styleBg = {
        backgroundImage: `url(${items[index + addingIndex].backgroundImage}`,
        backgroundPosition: 'center',
      };

      return (
        <li key={`workBackgroundDetail-${index}`}>
          <div
            style={styleBg}
            className={this.state.showItemDetail === index + addingIndex ? `${styles.detail} ${styles.show}` : styles.detail}
          />
        </li>
      );
    }, items);
  }

  _renderData(items) {
    return items.map((item, index) => {
      return (
        <li onMouseOver={(e) => { e.preventDefault(); this.handleMouseOver(index); }} key={`workDetail-${index}`}>
          <div
            className={this.state.showItemDetail === index ? `${styles.detail} ${styles.show}` : styles.detail}
          >
            <article>
              <div className={styles.contentWrapper}>
                <h2>{item.title}</h2>
                <p>{item.content}</p>
                <Button onButtonClick={ () => this.openLightbox(index) } className={styles.button}>SHOW DETAIL</Button>
              </div>
            </article>
          </div>
        </li>
      );
    });
  }

  render() {
    const { data } = this.state;
    const Gallery = (
      <Lightbox
        currentImage={this.state.currentLightboxImage}
        images={this.state.lightBoxGalleryImages[this.state.currentLightboxGalleryImagesSelected]}
        isOpen={this.state.lightboxIsOpen}
        onClickPrev={this.gotoPrevLightboxImage}
        onClickNext={this.gotoNextLightboxImage}
        onClose={this.closeLightbox}
        onClickThumbnail={this.gotoLightboxImage}
        style={{ position: 'relative', zIndex: '99999' }}
        showThumbnails
      />
    );

    const DetailBgRendered = (
      <ul className={styles.worksDetail}>
          {data ? this._renderBackgroundData(data) : this._renderLoading()}
      </ul>
    );

    return (
      <div>
        <ul className={styles.works} onMouseLeave={(e) => { e.preventDefault(); this.handleMouseOutOfSection(); }}>
          {data ? SectioWork._renderImages(data) : SectioWork._renderLoading()}
        </ul>
        { !IsMobile ? DetailBgRendered : null }
        <ul className={styles.worksDetail} onMouseLeave={(e) => { e.preventDefault(); this.handleMouseOutOfSection(); }}>
          {data ? this._renderData(data) : this._renderLoading()}
        </ul>
        {Gallery}
      </div>
    );
  }
}

SectioWork.defaultProps = {
  data: [],
};

SectioWork.propTypes = {
  data: React.PropTypes.arrayOf(React.PropTypes.object),
};

export default SectioWork;
