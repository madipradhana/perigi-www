import styles from './style.css';
import Heading from './sections/SectionHeading';
import Service from './sections/SectionService';
import Work from './sections/SectionWork';
import About from './sections/SectionAbout';
import Contact from './sections/SectionContact';
import PageSection from '~/src/client/ui/sections/ParallaxSection';

import logo from '~/src/client/assets/images/logo-dark.png';
import InitHeadingBgImage from '~/src/client/assets/images/bg-heading-initial.png';
import HeadingBgImage from '~/src/client/assets/images/bg-heading.png';
import InitServiceBgImage from '~/src/client/assets/images/bg-service.png';
import ServiceBgImage from '~/src/client/assets/images/bg-service.png';
import AboutBgImage from '~/src/client/assets/images/bg-about.png';
import IconService1 from '~/src/client/assets/images/service-1.png';
import IconService2 from '~/src/client/assets/images/service-2.png';
import IconService3 from '~/src/client/assets/images/service-3.png';

import WorkResidentialImage from '~/src/client/assets/images/work-residential.png';
import WorkOfficeImage from '~/src/client/assets/images/work-office.png';

import { anchorService, anchorAbout, anchorWork, anchorContact } from '../ViewMain/anchorTypes';

const worksResponse = [
  {
    backgroundImage: WorkResidentialImage,
    title: 'Residentials',
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
  },
  {
    backgroundImage: WorkOfficeImage,
    title: 'Offices',
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
  },
  {
    backgroundImage: WorkResidentialImage,
    title: 'Apartments',
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
  },
  {
    backgroundImage: WorkOfficeImage,
    title: 'Others',
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
  }
];

const response = {
  headingSection: {
    initialBackgroundImage: InitHeadingBgImage,
    backgroundImage: HeadingBgImage,
  },
  serviceSection: {
    serviceItems: [
      {
        icon: IconService1,
        title: 'Interior Design',
      },
      {
        icon: IconService2,
        title: 'Property Developer',
      },
      {
        icon: IconService3,
        title: 'Architecture',
      }
    ]
  },
  aboutSection: {
    contents: [
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
      'Hahahihi. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
    ],
    contacts: {
      info: 'Aliquam ac justo interdum, elementum ligula et, condimentum orci. Lorem ipsum dolor sit',
      phone: '+ 123-456-789-456',
      fax: '+ 123-456-789-456',
      workHours: {
        open: [
          'Monday - Friday : 09am - 18pm',
          'Saturday : 09am - 1pm'
        ],
        close: ['Sunday']
      }
    }
  }
}

class PageIndex extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { headingSection, serviceSection, aboutSection } = response;

    return (
      <div>
        <PageSection
          initialBackgroundImage={headingSection.initialBackgroundImage}
          backgroundImage={headingSection.backgroundImage}
          className={styles.headingSection}
        >
          <Heading />
        </PageSection>
        <PageSection
          anchorId={anchorService}
          initialBackgroundImage ={InitServiceBgImage}
          backgroundImage={ServiceBgImage}
          className={styles.serviceSection}
        >
          <Service items={serviceSection.serviceItems} isLoading={false} />
        </PageSection>
        <PageSection
          anchorId={anchorWork}
          className={styles.workSection}
        >
          <Work data={worksResponse} />
        </PageSection>
        <PageSection
          anchorId={anchorAbout}
          backgroundImage={AboutBgImage}
          className={styles.aboutSection}
        >
          <About contents={aboutSection.contents} contacts={aboutSection.contacts} isLoading={false} />
        </PageSection>
        <PageSection
          anchorId={anchorContact}
          className={styles.contactSection}
        >
          <Contact />
        </PageSection>
      </div>
    );
  }
}

export default PageIndex;
