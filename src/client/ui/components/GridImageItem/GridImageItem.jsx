import styles from './style.css';

const GridImageItem = ({ gridImage, children, className }) => {
  const cardStyle = className ? `${styles.card} ${className}` : styles.card;

  return (
    <div className={cardStyle}>
      <div><img src={gridImage} alt="grid-image" /></div>
      <h3>{children}</h3>
    </div>
  );
};

GridImageItem.propTypes = {
  gridImage: React.PropTypes.string,
  className: React.PropTypes.string,
  children: React.PropTypes.any,
};

export default GridImageItem;
